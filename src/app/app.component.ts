import { Component, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  @ViewChild('sidenav', {static: true}) sidenav: MatSidenav;
  links = [
    { url: 'about', name: 'About' },
    { url: 'credit-card', name: 'Tarjeta de Crédito' },
    { url: 'loan', name: 'Préstamo'}
  ];
}

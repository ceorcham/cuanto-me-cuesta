import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-loan',
  templateUrl: './loan.component.html',
  styleUrls: ['./loan.component.css']
})
export class LoanComponent implements OnInit {
  @Input() ammount: number;
  @Input() months = 60;
  @Input() monthlyPercent = 1.8;

  monthlyAmmount;
  shareLink;
  displayAmmount;
  displayMonths;
  displayMonthlyPercent;

  displayedColumns: string[] = ['month', 'capital', 'interest', 'total', 'left', 'interestSum'];
  payments = [];

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.queryParams.subscribe(q => {
      if (q.share) {
        try {
          [this.ammount, this.monthlyPercent, this.months] = <any>atob(decodeURIComponent(q.share)).split(';');
          this.calculatePayments();
        } catch (e) {
          console.log('Invalid share code');
        }
      }
    });
  }

  calculatePayments () {
    const P = this.ammount;
    const i = this.monthlyPercent / 100;
    const n = this.months;
    this.payments = this.calculateMonthlyPayments(P, i , n);
    this.displayAmmount = this.ammount;
    this.displayMonthlyPercent = this.monthlyPercent;
    this.displayMonths = this.months;
  }

  clearCalculation() {
    this.displayAmmount = undefined;
    this.displayMonthlyPercent = undefined;
    this.displayMonths = undefined;
    this.monthlyAmmount = undefined;
    this.payments = [];
  }

  copyShareLinkToClipboard() {
    const P = this.ammount;
    const i = this.monthlyPercent;
    const n = this.months;
    const baseUrl = document.location.origin + document.location.pathname;
    const shareLink = baseUrl + '?share=' + encodeURIComponent(btoa([P, i, n].join(';')));
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = shareLink;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.shareLink = shareLink;
    alert('Enlace para compartir copiado al portapapeles!');
  }

  /**
   * @param P The ammount of the loan
   * @param i The montly interest rate in decimal (ie. for 1% must be 0.01)
   * @param n The number of months of the loan
   * @returns the montly payment for the loan
   */
  private calculateMonthlyAmmount(P, i, n) {
    // R = P [(i (1 + i)^n) / ((1 + i)^n – 1)]
    return P * ( (i * Math.pow(1 + i, n) ) / (Math.pow(1 + i, n) - 1) );
  }

  /**
   * @param P The ammount of the loan
   * @param i The montly interest rate in decimal (ie. for 1% must be 0.01)
   * @param n The number of months of the loan
   * @returns The detailed montly payments for the loan
   */
  private calculateMonthlyPayments(P, i, n) {
    const payments = [];
    let monthlyAmmount = this.roundUp(this.calculateMonthlyAmmount(P, i, n), 2);
    this.monthlyAmmount = monthlyAmmount;

    let ammount = P;
    let interestSum = 0;
    for (let m = 1; m <= n; m++) {
      const interest = this.roundDown(ammount * i, 2);
      interestSum += interest;
      const capital = parseFloat((monthlyAmmount - interest).toFixed(2));
      ammount = ammount - capital;
      if (ammount < 0) {
        monthlyAmmount += ammount;
        ammount = 0;
      }
      payments.push({
        month: m,
        capital: capital,
        interest: interest,
        total: monthlyAmmount,
        left: ammount,
        interestSum: interestSum
      });
    }
    return payments;
  }

  /**
   * @param num The number to round up
   * @param precision The number of decimal places to preserve
   */
  private roundUp(num, precision) {
    precision = Math.pow(10, precision);
    return Math.ceil(num * precision) / precision;
  }

  /**
   * @param num The number to round down
   * @param precision The number of decimal places to preserve
   */
  private roundDown(num, precision) {
    precision = Math.pow(10, precision);
    return Math.floor(num * precision) / precision;
  }

}

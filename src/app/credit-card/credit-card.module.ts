import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreditCardRoutingModule } from './credit-card-routing.module';
import { CreditCardComponent } from './credit-card.component';

@NgModule({
  imports: [
    CommonModule,
    CreditCardRoutingModule
  ],
  declarations: [CreditCardComponent]
})
export class CreditCardModule { }

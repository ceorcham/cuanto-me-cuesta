import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'about',  loadChildren: './about/about.module#AboutModule' },
  { path: 'credit-card',  loadChildren: './credit-card/credit-card.module#CreditCardModule' },
  { path: 'loan',  loadChildren: './loan/loan.module#LoanModule' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
